    
var stompClient = null;
var res = null;

function setConnected(connected) {
    console.log("Set connected pressed")
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connectToServer() {
    var socket = new SockJS('https://swp.ddns.net:443/simulator');
    console.log("start")
    console.log(socket)
    stompClient = Stomp.over(socket);
    console.log(stompClient)
    stompClient.connect({}, function (frame) {
        setConnected(true);
        stompClient.subscribe('/data/', function (data) {
            var results = data.body;
            res = JSON.parse(results);
        });
    });
}

function getJSONData(){
    return res;
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}
