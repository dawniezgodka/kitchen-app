/*CREATE ALL VARIABLES */
var appliance = "";	
var nr = 0;
var sensor1 = {temperatur:[], druck:[],time:""};
var sensor2 = {temperatur:[],time:""};
var sensorlist = [sensor1,sensor2];

var dps_temp_show = [];
var dps_druck_show = [];

var dataLength = 50; // number of dataPoints visible at any point

var jsonObj = null;

var x = 0;

var sampling = 5000;

var newTemp = "No temperatur available";
var agMin = "No aggregation available";
var agHour = "No aggregation available";

//lamp
var offcolor = 'grey';
var oncolor = 'red';
var breakpoint = 25;


/*UPDATE ALL THE DATA */
function updateData(){
	jsonObj = getJSONData();
	if(jsonObj == null){
		return;
	}

	if('temperatur' in sensorlist[nr]){
		newTemp = jsonObj[nr].paramsVals.temperature +" °Celsius";
		if(jsonObj[nr].aggrVals.minute_aggregated_temperature!=""){
			agMin = jsonObj[nr].aggrVals.minute_aggregated_temperature +" °Celsius";
		}
		if(jsonObj[nr].aggrVals.hour_aggregated_temperature!=""){
			agHour = jsonObj[nr].aggrVals.hour_aggregated_temperature +" °Celsius";
		}
	}
	var newDruck ="No druck available";
	if('druck' in sensorlist[nr]){
		var newDruck = jsonObj[nr].paramsVals.pressure+" hP";
		if(jsonObj[nr].aggrVals.minute_aggregated_pressure!=""){
			agMin += " & "+jsonObj[nr].aggrVals.minute_aggregated_pressure+ " hP";
		}
		if(jsonObj[nr].aggrVals.hour_aggregated_pressure!=""){
			agHour += " & "+jsonObj[nr].aggrVals.hour_aggregated_pressure+ " hP";
		}
	}
	$('#tempId').html(newTemp);
	$('#druckId').html(newDruck);
	$('#zeitId').html(sensorlist[nr].time);
	$('#agMin').html(agMin);
	$('#agHour').html(agHour);
	if(parseFloat(jsonObj[nr].paramsVals.temperature)>=breakpoint){
		$('#powerLamp').css('color',oncolor);
	}else{
		$('#powerLamp').css('color',offcolor);
	}

	updateSensorlistData();
}


/*Load sensor data into sensor objects */
function updateSensorlistData(){
	for (i = 0; i <jsonObj.length;i++){
		if('temperatur' in sensorlist[i]){
			sensorlist[i].temperatur.push({x:x,y:parseFloat(jsonObj[i].paramsVals.temperature.replace(',', '.'))});
			//Shift temp if too many entries
			if(sensorlist[i].temperatur.length >dataLength){
				sensorlist[i].temperatur.shift();
			}
		}
		if('druck' in sensorlist[i]){
			sensorlist[i].druck.push({x:x,y:parseFloat(jsonObj[i].paramsVals.pressure.replace(',', '.'))});
			//Shift druck if too many entries
			if(sensorlist[i].druck.length >dataLength){
				sensorlist[i].druck.shift();
			}
		}
		sensorlist[i].time = jsonObj[i].measurementTime.time.hour+":"+jsonObj[i].measurementTime.time.minute+" Uhr "+jsonObj[i].measurementTime.time.second+" Seconds";

	}
	x+=sampling/1000;

	//copy data into graph
	if('temperatur' in sensorlist[nr]){
	 	$.extend(dps_temp_show,sensorlist[nr].temperatur);
	 	chart_temp.render();
	};
	if('druck' in sensorlist[nr]){
		$.extend(dps_druck_show,sensorlist[nr].druck);
		chart_druck.render();
	};
}

/*Change the Application: Water-Heater, Toaster etc. */
function openAppliance(app, appNr){
	nr = parseInt(appNr);
	if(appliance!=""){
		$('#'+appliance).css('background-color','');
		$('#'+appliance).css('color','black');
		$('#'+appliance).css('font-size','');
	}
	appliance = app;
	$('#'+appliance).css('background-color','#3b5924');
	$('#'+appliance).css('color','white');
	$('#'+appliance).css('font-size','20px');
	$('#powerLamp').css('color',offcolor);
	if($("#canvasDiv").attr("style")==""){
		$("#canvasDiv").attr("style","opacity:1");
	}
	//check what data sensor has
	if('temperatur' in sensorlist[nr]){ $("#tempbtn").css('display','block') }else{$("#tempbtn").css('display','none')};
	if('druck' in sensorlist[nr]){ $("#druckbtn").css('display','block') }else{$("#druckbtn").css('display','none')};	

	//reset datafields
	$('#tempId').html("---");
	$('#druckId').html("---");
	$('#agMin').html("---");
	$('#agHour').html("---");

	//dont show graphs when open a new tab
	if($("#tempDiv").css("display")=="block"){$("#tempbtn").click()};
	if($("#druckDiv").css("display")=="block"){$("#druckbtn").click()};	
}

/*create graphs for the first time*/
function createGraphs(){
	chart_temp = new CanvasJS.Chart("chartContainer", {
		title :{
			text: "Temperaturverlauf"
		},
		axisY: {
			title: "Temperature (in °C)",
			includeZero: false
		},     
		axisX: {
			title: "Seconds since start",
		},
		data: [{
			type: "line",
			dataPoints: dps_temp_show
		}]
	});
	chart_druck = new CanvasJS.Chart("chartContainer2", {
		title :{
			text: "Druckverlauf"
		},
		axisY: {
			title: "Druch (in hPa)",
			includeZero: false
		},     
		axisX: {
			title: "Seconds since start",
		},
		data: [{
			type: "line",
			dataPoints: dps_druck_show
		}]
	});	
}


