function setCookie(bp) {
  var d = new Date();
  d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = "breakpoint=" + bp + ";" +  expires + ";path=/";
}


function isCookie() {
  var cookie = getCookie("swp-cookie");
  if (cookie != "") {
    return true;
  }else{
    return false;
  }
} 

function getCookie() {
  var ca = document.cookie.split(';');
  return ca[0];
}