<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand" href="#">Kitchen Appliance App</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item">
			<a id="homeLink" class="nav-link" href="#"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
		  </li>
		  <li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			  <i class="fas fa-database"></i> More interesting data
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
			  <a class="dropdown-item" href="#">All temperature data</a>
			  <a class="dropdown-item" href="#">All pressure data</a>
			  <a class="dropdown-item" href="#">Long term data</a>
			</div>
		  </li>
		  <li class="nav-item">
			<a id="aboutUsLink" class="nav-link" href="#"><i class="fas fa-user-friends"></i> About Us</a>
		  </li>	
		  <li class="nav-item">
			<a id="" class="nav-link" href="#" data-toggle="modal" data-target="#settingsModal"><i class="fas fa-cog"></i> Settings</a>
		  </li>	
		</ul>
	  </div>
	</nav>
	
	<div class="main">
		<div id="tabs">
			<div id="firstContentDiv" class="invisDiv">
				<?php include "content/main.html"; ?>
			</div>
			<div id="secondContentDiv" class="invisDiv">
				<?php include "content/alltemperature.html"; ?>
			</div>
			<div id="thirdContentDiv" class="invisDiv">
				<?php include "content/aboutus.html"; ?>
			</div>
		</div>
	</div>

	<div class="modal" id="settingsModal">
	  <div class="modal-dialog">
	    <div class="modal-content">

	      <!-- Modal Header -->
	      <div class="modal-header">
			<h3 class="modal-title"><i class="fas fa-cog"></i> </h3>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>

	      <!-- Modal body -->
	      <div class="modal-body">
	      	<h4>Lamp settings:</h4>
	        <form>
			  <div class="form-group">
			    <label for="exampleFormControlInput1">Break point Off/On:</label>
			    <input id="settingsNr" type="number" name="quantity" min="1" max="99" placeholder="25">
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlSelect1">Off/On Color:</label>
			    <select class="form-control" id="settingsForm">
			      <option>grey/red</option>
			      <option>grey/orange</option>
			      <option>grey/green</option>
			      <option>black/orange</option>
			    </select>
			  </div>
			 </form>
			 <hr>
			<h4>Other settings:</h4>
			#TODO:
			<ul>
				<li>Set interval</li>
				<li>...</li>
			</ul>
	      </div>

	      <!-- Modal footer -->
	      <div class="modal-footer">
	        <button id="settingsBtn" type="button" class="btn btn-success" data-dismiss="modal">Save</button>
	      </div>

	    </div>
	  </div>
	</div>

	<div class="footer">
  		<p><a href="content/datenschutz.html">Datenschutzerklärung</a></p>
	</div>
		
		
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.js" type="text/javascript"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.8.2/js/all.js" integrity="sha384-DJ25uNYET2XCl5ZF++U8eNxPWqcKohUUBUpKGlNLMchM7q4Wjg2CUpjHLaL8yYPH" crossorigin="anonymous"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	<script src="js/connection.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.3.0/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/cookies.js"></script>
	<script>

		var openDiv = "";

		/*START WHEN DOCUMENT IST READY*/
		$( document ).ready(function() {

			//Check and load cookie data
			if(isCookie()){
				var bp = getCookie().split("=");
				$("#breakpointId").html("Breakpoint at "+bp[1]);
				breakpoint = parseFloat(bp[1]);

			}

			connectToServer();
			setInterval(updateData, sampling); //Call update every sampling time

			createGraphs();

			$('#Water-Heater').click(); //Open Water-heater
			addOnBtnClickFunctions();	

			$("#homeLink").click()		
		});

		$("#homeLink").click(function(){
			if(openDiv !=""){
				$(openDiv).fadeToggle();
			}
			$("#firstContentDiv").fadeToggle();
			openDiv = "#firstContentDiv";
		});

		$("#aboutUsLink").click(function(){
			if(openDiv !=""){
				$(openDiv).fadeToggle();
			}
			$("#thirdContentDiv").fadeToggle();
			openDiv = "#thirdContentDiv";
		});

		

		/*add btn click function*/
		function addOnBtnClickFunctions(){		
			$("#tempbtn").click(function(){
				if($("#tempDiv").css("display")=="block"){				
					$("#tempDiv").css("display","none");
				}else{
					$("#tempDiv").css("display","block")
				}
				updateData();
			});		

			$("#druckbtn").click(function(){
				if($("#druckDiv").css("display")=="block"){				
					$("#druckDiv").css("display","none");
				}else{
					$("#druckDiv").css("display","block")
				}
				updateData();
			});
			$("#settingsBtn").click(function(){
				var bp = $("#settingsNr").val();
				console.log("BREAKPOINT: "+bp);
				if(/^\d+$/.test(bp)){
					setCookie(bp);
					$("#breakpointId").html("Breakpoint at "+bp);
					breakpoint = parseFloat(bp);
				}else{
					alert("FAILED! Only digits allowed");
				}

			})

		}
	</script>
</body>
</html>