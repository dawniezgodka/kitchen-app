package app.message;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
public class Message {

    private String id;
    private LocalDateTime measurementTime;
    private ConcurrentHashMap<String, String> paramsVals;
    private ConcurrentHashMap<String, String> aggrVals;


    public Message(String id, LocalDateTime measurementTime,
                   ConcurrentHashMap<String, String> paramsVals,
                   ConcurrentHashMap<String, String> aggrVals) {
        this.id = id;
        this.measurementTime = measurementTime;
        this.paramsVals = paramsVals;
        this.aggrVals = aggrVals;
    }
}
