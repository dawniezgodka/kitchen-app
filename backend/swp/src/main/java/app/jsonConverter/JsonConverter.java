package app.jsonConverter;

import app.sensors.Sensor;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JsonConverter implements Converter<Sensor> {

    @Override
    public String makeFromList(List<Class<? extends Sensor>> sensors) {
        return new Gson().toJson(sensors);
    }
}
