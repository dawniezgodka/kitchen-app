package app.jsonConverter;

import java.util.List;

public interface Converter<A> {

    String makeFromList(List<Class<? extends A>> objs);
}
