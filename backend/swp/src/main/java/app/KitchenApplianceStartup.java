package app;

import app.sensors.AllDetectingDevices;
import app.sensors.Bme280Sensor;
import app.sensors.OneWireSensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class KitchenApplianceStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    AllDetectingDevices allDevices;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        Bme280Sensor bme280Sensor = new Bme280Sensor("88", 1024, "127.0.0.1", "celsius",
                    "hPa");

        OneWireSensor oneWireSensor = new OneWireSensor("28-04163410e2ff", 1025,
                    "127.0.0.1", "celsius");

        allDevices.addSensor(bme280Sensor);
        allDevices.addSensor(oneWireSensor);

    }
}
