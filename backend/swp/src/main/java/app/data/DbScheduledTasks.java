package app.data;

import app.models.*;
import app.repos.AggregatedMeasurementRepository;
import app.repos.DailyAggregationRepository;
import app.repos.InstantMeasurementRepository;

import app.repos.WeeklyAggregationRepository;
import app.sensors.AllDetectingDevices;
import app.sensors.Sensor;
import app.tools.Rounder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DbScheduledTasks  {

    private static final int SECOND = 1000;
    private static final int HOUR = SECOND*60*60;
    private static final int DAY = HOUR*24;
    private static final int WEEK = DAY*7;

    @Autowired
    private InstantMeasurementRepository instantRepo;

    @Autowired
    private AggregatedMeasurementRepository aggregatedRepo;

    @Autowired
    private DailyAggregationRepository dailyAggregationRepository;

    @Autowired
    private WeeklyAggregationRepository weeklyAggregationRepository;

    @Autowired
    private AllDetectingDevices persistenceDevices;

    @Scheduled(fixedRate = SECOND)
    void executeShortTermPersistenceTask() {
        for (Sensor s : persistenceDevices.getSensors()) {
            s.checkConnectivity();
            if (s.isConnected()) {
                String payload = s.readFromSocket(s.getHost(), s.getPort());
                if (payload.equals("")) continue;
                Map<String, String> dataFromSocket = getMapFromDataReadFromSocket(payload);
                List<InstantMeasurement> measurements = processMapAndPrepareRecordsForPersistence(dataFromSocket);
                for (InstantMeasurement im : measurements) {
                    instantRepo.save(im);
                }
            } else {
                continue;
            }
        }
    }

    @Scheduled(fixedRate = SECOND*60, initialDelay = SECOND*61)
    void minuteAggregationForTesting() {
        Sensor sensorToBeUpdated = null;
        List<Aggregation> aggregationResult = instantRepo.determineMeanForLastMinute();
        for (Aggregation singleAggr : aggregationResult) {
            AggregatedMeasurement am = new AggregatedMeasurement();
            am.setSensorId(singleAggr.getSensorId());
            am.setAggregatedValue(singleAggr.getMean());
            am.setMeasurementType(UnitType.valueOf(singleAggr.getType()));
            String key = "minute_aggregated_"+singleAggr.getType().toLowerCase();
            sensorToBeUpdated = persistenceDevices.findSensorById(singleAggr.getSensorId());
            sensorToBeUpdated.getAggrValues().put(key,
                    Rounder.getTwoDecimals(singleAggr.getMean()));
        }
    }

    @Scheduled(fixedRate = HOUR, initialDelay = HOUR + SECOND)
    void executeDeleteTask() {
        Sensor sensorToBeUpdated = null;
        List<Aggregation> aggregationResult = instantRepo.determineMeanForLastHourRecords();
        for (Aggregation singleAggr : aggregationResult) {
            AggregatedMeasurement am = new AggregatedMeasurement();
            am.setSensorId(singleAggr.getSensorId());
            am.setAggregatedValue(singleAggr.getMean());
            am.setMeasurementType(UnitType.valueOf(singleAggr.getType()));
            aggregatedRepo.save(am);
            String key = "hour_aggregated_"+singleAggr.getType().toLowerCase();
            sensorToBeUpdated = persistenceDevices.findSensorById(singleAggr.getSensorId());
            sensorToBeUpdated.getAggrValues().put(key,
                    Rounder.getTwoDecimals(singleAggr.getMean()));
        }
        instantRepo.deleteLastHourRecordsForEachId();

    }

    @Scheduled(fixedRate = DAY, initialDelay = DAY + SECOND)
    void executeDailyAggregationTask() {
        Sensor sensorToBeUpdated = null;
        List<IDailyAggregation> aggregationResult = aggregatedRepo.determineMeanForLast24Hours();
        for (IDailyAggregation singleAggr : aggregationResult) {
            AggregatedMeasurement am = new AggregatedMeasurement();
            am.setSensorId(singleAggr.getSensorId());
            am.setAggregatedValue(singleAggr.getMean());
            am.setMeasurementType(UnitType.valueOf(singleAggr.getType()));
            dailyAggregationRepository.save(am);
            String key = "day_aggregated_"+singleAggr.getType().toLowerCase();
            sensorToBeUpdated = persistenceDevices.findSensorById(singleAggr.getSensorId());
            sensorToBeUpdated.getAggrValues().put(key,
                    Rounder.getTwoDecimals(singleAggr.getMean()));

        }
    }

    @Scheduled(fixedRate = WEEK, initialDelay = WEEK + SECOND)
    void executeWeeklyAggregationTask() {
        Sensor sensorToBeUpdated = null;
        List<IWeeklyAggregation> aggregationResult = dailyAggregationRepository.determineMeanForLastWeek();
        for (IWeeklyAggregation singleAggr : aggregationResult) {
            AggregatedMeasurement am = new AggregatedMeasurement();
            am.setSensorId(singleAggr.getSensorId());
            am.setAggregatedValue(singleAggr.getMean());
            am.setMeasurementType(UnitType.valueOf(singleAggr.getType()));
            weeklyAggregationRepository.save(am);
            String key = "week_aggregated_"+singleAggr.getType().toLowerCase();
            sensorToBeUpdated = persistenceDevices.findSensorById(singleAggr.getSensorId());
            sensorToBeUpdated.getAggrValues().put(key,
                    Rounder.getTwoDecimals(singleAggr.getMean()));

        }
    }

    private Map<String, String> getMapFromDataReadFromSocket(String jsonString) {
        Type mapType = new TypeToken<Map<String, String>>(){}.getType();
        return new Gson().fromJson(jsonString, mapType);
    }

    //TODO: make this logic more reusable
    private List<InstantMeasurement> processMapAndPrepareRecordsForPersistence(
            Map<String, String> data
    ) {
        List<InstantMeasurement> result = new ArrayList<>();
        if (data.size() > 2) {
            InstantMeasurement temp = new InstantMeasurement();
            InstantMeasurement pres = new InstantMeasurement();
            temp.setSensorId(data.get("id"));
            pres.setSensorId(data.get("id"));
            temp.setInstantValue(Double.parseDouble(data.get("temperature")));
            pres.setInstantValue(Double.parseDouble(data.get("pressure")));
            temp.setMeasurementType(UnitType.TEMPERATURE);
            pres.setMeasurementType(UnitType.PRESSURE);
            result.add(temp);
            result.add(pres);

        } else {
            InstantMeasurement singleMeasurement = new InstantMeasurement();
            singleMeasurement.setSensorId(data.get("id"));
            singleMeasurement.setInstantValue(Double.parseDouble(data.get("temperature")));
            singleMeasurement.setMeasurementType(UnitType.TEMPERATURE);
            result.add(singleMeasurement);
        }
        return result;
    }
}
