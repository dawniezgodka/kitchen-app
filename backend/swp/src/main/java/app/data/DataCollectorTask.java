package app.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

public class DataCollectorTask implements Runnable {

    @Autowired
    private SensorService sensorService;

    @Autowired
    private SimpMessagingTemplate template;

    @Override
    public void run() {
        String info = sensorService.prepareInformation();
        System.out.println(info);
        template.convertAndSend("/data/", info);
    }
}
