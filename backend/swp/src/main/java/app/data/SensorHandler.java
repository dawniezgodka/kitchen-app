package app.data;

import app.jsonConverter.Converter;
import app.message.Message;
import app.sensors.AllDetectingDevices;
import app.sensors.Sensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SensorHandler implements SensorService {

    @Autowired
    private Converter converter;

    @Autowired
    private AllDetectingDevices detectingDevices;

    @Override
    public String prepareInformation() {
        List<Message> sensorMessages = new ArrayList<>();
        for (Sensor s : detectingDevices.getSensors()) {
            s.checkConnectivity();
            if(s.isConnected()) {
                String payload = s.readFromSocket(s.getHost(), s.getPort());
                if (payload.equals("")) {
                    ConcurrentHashMap<String, String> noData = new ConcurrentHashMap<>();
                    sensorMessages.add(new Message(s.getId(), LocalDateTime.now().plusHours(1),
                            noData, s.getAggrValues()));
                } else {
                    s.setMeasuredParameterValue(payload);
                    Message m = s.prepareMsg(s.getId(), LocalDateTime.now().plusHours(1),
                            s.getParams(), s.getAggrValues());

                    sensorMessages.add(m);

                }
            } else {
                ConcurrentHashMap<String, String> noData = new ConcurrentHashMap<>();
                sensorMessages.add(new Message(s.getId(), LocalDateTime.now().plusHours(1),
                        noData, s.getAggrValues()));
            }
        }
        return converter.makeFromList(sensorMessages);
    }
}
