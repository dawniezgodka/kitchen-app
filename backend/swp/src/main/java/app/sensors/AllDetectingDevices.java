package app.sensors;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AllDetectingDevices {

    private static AllDetectingDevices collection;
    private List<Sensor> sensors;

    private AllDetectingDevices() {
        sensors = new ArrayList<Sensor>();
    }

    public static AllDetectingDevices getCollection() {
        if (AllDetectingDevices.collection == null) {
            AllDetectingDevices.collection = new AllDetectingDevices();
        }
        return AllDetectingDevices.collection;
    }

    public List<Sensor> getSensors() {
        return this.sensors;
    }

    public void addSensor(Sensor s) {
        this.sensors.add(s);
    }

    public Sensor findSensorById(String id) {
        for (Sensor s : sensors) {
            if (s.getId().equals(id)) {
                return s;
            }
        }
        return sensors.get(0);
    }
}
