package app.sensors;

import app.tools.Rounder;
import lombok.Getter;
import lombok.Setter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
public class Bme280Sensor extends Sensor {

    private String tempUnit;
    private String pressureUnit;
    private double tempValue;
    private double pressureValue;

    public Bme280Sensor(String id, int port, String host,
                        String tempUnit, String pressureUnit) {
        super(id, port, host);
        this.pressureUnit = pressureUnit;
        this.tempUnit = tempUnit;
        super.getAggrValues().put("minute_aggregated_temperature","");
        super.getAggrValues().put("hour_aggregated_temperature","");
        super.getAggrValues().put("day_aggregated_temperature","");
        super.getAggrValues().put("week_aggregated_temperature","");
        super.getAggrValues().put("minute_aggregated_pressure","");
        super.getAggrValues().put("hour_aggregated_pressure","");
        super.getAggrValues().put("day_aggregated_pressure","");
        super.getAggrValues().put("week_aggregated_pressure","");
    }

    @Override
    public void setMeasuredParameterValue(String dataReadFromSocket) {

        Map<String, String> measurementMap = getMapFromDataReadFromSocket(dataReadFromSocket);
        super.getParams().put("temperature", Rounder.getTwoDecimals(measurementMap.get("temperature")));
        super.getParams().put("pressure", Rounder.getTwoDecimals(measurementMap.get("pressure")));
    }
}
