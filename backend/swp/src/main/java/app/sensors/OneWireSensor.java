package app.sensors;


import app.tools.Rounder;
import lombok.Getter;
import lombok.Setter;
import java.util.Map;

@Getter
@Setter
public class OneWireSensor extends Sensor {

    private String tempUnit;
    private String tempValue;

    public OneWireSensor(String id, int port, String host,
                        String tempUnit) {
        super(id, port, host);
        this.tempUnit = tempUnit;
        super.getAggrValues().put("minute_aggregated_temperature","");
        super.getAggrValues().put("hour_aggregated_temperature","");
        super.getAggrValues().put("day_aggregated_temperature","");
        super.getAggrValues().put("week_aggregated_temperature","");
    }

    @Override
    public void setMeasuredParameterValue(String dataReadFromSocket) {
        Map<String, String> measurementMap = getMapFromDataReadFromSocket(dataReadFromSocket);
        super.getParams().put("temperature", Rounder.getTwoDecimals(measurementMap.get("temperature")));
    }
}
