package app.sensors;

import app.message.Message;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
public abstract class Sensor {

    private String id;

    private String host;
    private int port;
    private boolean isConnected = false;

    private Socket socket;

    private ConcurrentHashMap<String, String> params;
    private ConcurrentHashMap<String, String> aggrValues = new ConcurrentHashMap<>();

    public Sensor() {}

    public Sensor(String id, String measurementUnit, int port, String host) {
        this.id = id;
        this.port = port;
        this.host = host;
    }

    public Sensor(String id, int port, String host) {
        this.id = id;
        this.port = port;
        this.host = host;

    }

    public void checkConnectivity() {
        socket = null;
        try {
            socket = new Socket(host, port);
            isConnected = true;
            params = new ConcurrentHashMap<>();
        } catch (IOException e) {
            isConnected = false;
        }
    }

    public String readFromSocket(String host, int port) {
        String readData = "";
        try {
            BufferedReader reader = null;
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            readData = reader.readLine();
            out.flush();
        } catch (IOException | NullPointerException e) {
        } finally {
            return readData;
        }
    }

    public abstract void setMeasuredParameterValue(String dataReadFromSocket);

    public Message prepareMsg(String id, LocalDateTime measurementTime,
                              ConcurrentHashMap<String, String> params,
                              ConcurrentHashMap<String, String> aggrVals) {
        return new Message(id, measurementTime, params, aggrVals);
    }

    protected Map<String, String> getMapFromDataReadFromSocket(String jsonString) {
        Type mapType = new TypeToken<Map<String, String>>(){}.getType();
        return new Gson().fromJson(jsonString, mapType);
    }
}
