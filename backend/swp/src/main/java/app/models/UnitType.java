package app.models;

public enum UnitType {
    TEMPERATURE,
    PRESSURE,
    AIRPRESSURE,
    CURRENT
}
