package app.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "aggr_measurements")
@Getter
@Setter
public class AggregatedMeasurement extends Measurement {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "aggregated_value")
    private Double aggregatedValue;

}

