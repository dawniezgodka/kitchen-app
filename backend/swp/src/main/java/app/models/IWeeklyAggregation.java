package app.models;

public interface IWeeklyAggregation {

    String getSensorId();
    String getType();
    Double getMean();
}
