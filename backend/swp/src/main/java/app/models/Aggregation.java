package app.models;

public interface Aggregation {

    String getSensorId();
    String getType();
    Double getMean();
}
