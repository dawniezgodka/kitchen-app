package app.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "measurements")
@Getter
@Setter
public class InstantMeasurement extends Measurement {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "measurement_value")
    private Double instantValue;

}

