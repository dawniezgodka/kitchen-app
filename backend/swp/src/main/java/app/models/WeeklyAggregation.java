package app.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "weekly_meas")
@Getter
@Setter
public class WeeklyAggregation {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "aggr_val")
    private Double aggregatedValue;
}
