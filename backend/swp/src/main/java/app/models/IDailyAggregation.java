package app.models;

public interface IDailyAggregation {

    String getSensorId();
    String getType();
    Double getMean();
}
