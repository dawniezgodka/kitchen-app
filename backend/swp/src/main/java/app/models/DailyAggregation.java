package app.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "daily_meas")
@Getter
@Setter
public class DailyAggregation extends Measurement {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "aggr_val")
    private Double aggregatedValue;

}
