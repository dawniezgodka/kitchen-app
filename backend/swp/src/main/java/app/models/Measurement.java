package app.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public abstract class Measurement {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "measurement_time", nullable = false, updatable = false)
    @CreatedDate
    private Date createdAt;

    @Column(name="sensor_id")
    private String sensorId;

    @Enumerated(EnumType.STRING)
    @Column(name = "measurement_type")
    private UnitType measurementType;
}
