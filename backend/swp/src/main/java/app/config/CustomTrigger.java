package app.config;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import java.util.Date;

public class CustomTrigger implements DynamicSchedule, Trigger {

    private int interval = 5000;

    @Override
    public Date nextExecutionTime(TriggerContext triggerContext) {
        Date lastTime = triggerContext.lastActualExecutionTime();
        return (lastTime == null) ? new Date() : new Date(lastTime.getTime() + interval);
    }

    @Override
    public void changeInterval(int newInterval) {
        this.interval = newInterval;
    }

}
