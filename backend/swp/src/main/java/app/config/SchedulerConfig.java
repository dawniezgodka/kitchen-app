package app.config;

import app.data.DataCollectorTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
@EnableScheduling
public class SchedulerConfig implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskScheduler());
        taskRegistrar.addTriggerTask(dataCollectorTask(), myTrigger());
    }

    @Bean
    public Executor taskScheduler() {
        return Executors.newScheduledThreadPool(2);
    }

    @Bean
    public CustomTrigger myTrigger() {
        return new CustomTrigger();
    }

    @Bean
    public DataCollectorTask dataCollectorTask() {
        return new DataCollectorTask();
    }
}
