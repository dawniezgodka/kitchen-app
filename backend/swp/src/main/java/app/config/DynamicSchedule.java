package app.config;

public interface DynamicSchedule {

    void changeInterval(int millis);
}
