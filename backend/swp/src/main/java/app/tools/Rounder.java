package app.tools;

import java.text.DecimalFormat;

public class Rounder {

    private Rounder() {}

    public static String getTwoDecimals(double value){
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(value);
    }

    public static String getTwoDecimals(String value){
        Double d = Double.valueOf(value);
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(d);
    }

    public static String getTwoDecimals(int value){
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(value);
    }
}
