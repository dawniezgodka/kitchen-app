package app.repos;

import app.models.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DailyAggregationRepository extends JpaRepository<AggregatedMeasurement, Long> {

    @Query(value = "SELECT sensor_id AS sensorId, measurement_type as type, AVG(aggr_val) as val " +
            "FROM daily_meas " +
            "WHERE measurement_time >= (NOW() - INTERVAL '1 WEEK') " +
            "GROUP BY sensor_id, measurement_type",
            nativeQuery = true)
    List<IWeeklyAggregation> determineMeanForLastWeek();


}
