package app.repos;

import app.models.AggregatedMeasurement;
import app.models.Aggregation;
import app.models.IDailyAggregation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface AggregatedMeasurementRepository
        extends JpaRepository<AggregatedMeasurement, Long> {

    @Query(value = "SELECT sensor_id AS sensorId, measurement_type as type, AVG(aggregated_value) as val " +
            "FROM aggr_measurements " +
            "WHERE measurement_time >= (NOW() - INTERVAL '1 DAY') " +
            "GROUP BY sensor_id, measurement_type",
            nativeQuery = true)
    List<IDailyAggregation> determineMeanForLast24Hours();


}
