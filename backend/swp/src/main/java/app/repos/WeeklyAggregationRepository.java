package app.repos;

import app.models.AggregatedMeasurement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeeklyAggregationRepository extends JpaRepository<AggregatedMeasurement, Long> {}
