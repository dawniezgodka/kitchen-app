package app.repos;

import app.models.Aggregation;
import app.models.InstantMeasurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface InstantMeasurementRepository
        extends JpaRepository<InstantMeasurement, Long> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM measurements WHERE measurement_time >= (NOW() - INTERVAL '1 hour')",
            nativeQuery = true)
    void deleteLastHourRecordsForEachId();

    @Query(value = "SELECT sensor_id AS sensorId, measurement_type as type, AVG(measurement_value) as mean " +
            "FROM measurements " +
            "WHERE measurement_time >= (NOW() - INTERVAL '1 HOUR') " +
            "GROUP BY sensor_id, measurement_type",
            nativeQuery = true)
    List<Aggregation> determineMeanForLastHourRecords();

    @Query(value = "SELECT sensor_id AS sensorId, measurement_type as type, AVG(measurement_value) as mean " +
            "FROM measurements " +
            "WHERE measurement_time >= (NOW() - INTERVAL '1 MINUTE') " +
            "GROUP BY sensor_id, measurement_type",
            nativeQuery = true)
    List<Aggregation> determineMeanForLastMinute();
}
