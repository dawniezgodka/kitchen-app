%!TEX root = ../report.tex

\subsection{Introduction}
The design and implementation of a backend encompassed creating a system that acts as a link between sensors and the Frontend. More specifically, the following functional requirements were laid down for the Backend:
\begin{enumerate}
    \item Ensure seamless flow of information within the application,
    \item Provide a so-called statistics module that forms a basis for aggregated data visualisation,
    \item Provide a user the possibility to update the interval (a rate of receiving new information).
\end{enumerate}

Furthermore, the Backend has to fulfil some non-functional requirements such as stability of the system - if a sensor knocks off, the application has to keep going. 

\subsection{Technology}

The following criteria were considered in the search for the most suitable technology for the project: 
\begin{enumerate}
    \item Our programming experience with a particular technology,
    \item The estimated simplicity of implementing the concept,
    \item Support for other technologies that fell within the concept (for example RDBMS or websocket support).
\end{enumerate}

Two juxtaposed candidates were Java Spring Boot 2.0\footnote{https://spring.io/projects/spring-boot} and Python Django\footnote{https://www.djangoproject.com/}. 
After a short brainstorming, the decision was made to implement the Backend by means of Spring Boot 2.0, mainly because of the experience that came along with the technology. Furthermore, the framework provides all necessary tools that were indispensable to the implementation of the concept. It also facilitates the programming process thanks to the heavy use of annotations and straightforward configuration.

\subsection{Implementation - Requirements Fulfilment}
\subsubsection{Ensuring flow of information}

Providing a smooth exchange of data between sensors and the Frontend involved creating two communication channels withing the application as well as scheduling tasks responsible for collecting and processing data.

Two aforementioned communication channels are:

\begin{enumerate}
    \item a channel between the sensors and the Backend,
    \item a channel between the Backend and the Frontend.
\end{enumerate}

The communication between the sensors and the Backend was kept by means of a TCP socket. Every x\footnote{In this part of the report, for the sake of brevity, we use the term "x seconds" to replace rather verbose "every by-a-user-defined number of second". The default value was set to three seconds.} seconds the Backend acts as a client and asks both sensors for the current measurement. Before data is read from the socket, the availability of each sensor is verified. If a sensor is unreachable for some reason, the Backend behaves appropriately and prepares an empty response. Thus, the continuity of the application is maintained, and it functions even in case of non-availability of the sensors. 

As far as the communication between the Backend and the Frontend is concerned, we decided to employ websockets as a basis for exchanging data. The main reason behind the decision is the possibility of duplex communication, modest latency, and the lack of necessity of making a handshake every time the data is exchanged. The websocket protocol allows data to be sent from the server to the client and other way around at any time, without the need for making requests, sending responses, or polling the data. We decided that this communication protocol suits perfectly to our scenario: a server (the Backend) sends sensor data to the client (the Frontend), a client can send data containing various settings such as an update interval or requested aggregation type (to be implemented in the future).
Moreover, because of the low-level nature of websockets, we used STOMP\footnote{https://stomp.github.io/} as a sub-protocol in order to facilitate the communication between a broker (an emitter of messages) and a client (a receiver of messages). The lack of such a sub-protocol in a data exchange between a broker and a client written in different programming languages would entail writing a lot of complex, additional code.

Implementation of the first channel involved creating a model for sensors (designed by means of polymorphism and abstraction), network programming (connecting and receiving data from TCP sockets), processing the received data, and making suitable error handling (for example in a case of non-availability of a sensor).

Programming second channel took in configuring the Backend to use the websocket protocol as well as the STOMP (adding endpoints, setting allowed origins etc.). Furthermore, for this part, the Backend had to be set up to acts as a broker emitting appropriately formed messages. Since the STOMP is a text-based protocol, it was not possible to transfer a pure json object to the Frontend. We had to operate on json strings instead.

In order to create a real-time application it was not enough to just define two communication channels. There was a clear need for a mechanism that intertwines two channels and exposes real-time data coming in a particular interval to a user. The idea was to abstract the workings of the Backend to a single, scheduled task that gets executed every x seconds. Each time the task is performed, the Backend asks sensors for the current measurement, processes the responses, forms a message containing all relevant information and pushes it as a json string so that the Frontend that subscribes to the defined topic receives information immediately and displays it to a user.

From the programming point of view, creating a scheduled task involved becoming acquainted with the details of scheduling; forming a message took in json manipulations; sending data to the Frontend carried using the messaging template and pushing the information to the broker so that it can be consumed by the Frontend.

\subsubsection{Statistics module}

The primary goal of the statistics module was to provide insights into average exploitation of household devices.
The way to fulfil this requirement and show results to a user changed throughout the project. What stayed the same, was the idea to gather fine-grained measurements, save them in a database, aggregate after a specified amount of time, process a result, and delete short-term measurements so that limited memory is not squandered by keeping superfluous records. What changed was the way we processed results. At first, our idea was to keep aggregated results in a separate, long-term database and provide the numbers when requested by a user. However, mostly because of the lack of time, we were forced to abandon this idea. We decided that results of various aggregations (we made an aggregation every minute, hour, day, and week) will be immediately sent to the Frontend and displayed to the user.

Programmatically, this part entailed configuring a database and interacting with it as well as creating a model for an aggregation. We decided to store the data in Postgresql\footnote{https://www.postgresql.org/} database. 

The choice of the database system was rather arbitrary, and the main reason behind the Postgres was the fact that Spring Boot offers a native support for this system. Making four different aggregation levels involved creating four separate scheduled tasks. Meeting in a programmatically elegant way the requirement that the result of an aggregation has to be interwoven into the regular payload turned out to be much more difficult than we would have expected. The faced difficulty was caused by earlier design of the system: sending sensors data to the Frontend and working with the database were conceptualized as two independent tasks, whereas the imposed requirement forced the two to communicate with each other. We responded to this challenge by changing a little bit the structure of the sensors model, harnessing the dependency injection and concurrent hashmaps.

\subsubsection{Custom Interval}

Giving a user the ability to control the interval in which data comes was not a trivial task. Even though Spring Boot has embedded mechanism of scheduling tasks, it only allows to schedule tasks at fixed rate. In order to customize it, it was necessary to delve into low-level details of scheduling such as creating custom triggers, setting executors, registering and adding tasks.
Unfortunately, we didn't manage to use this ability in the final version of the application. Again, mainly because of the lack of time.

\subsubsection{Miscellaneous}

Beside three main requirements discussed in previous subsections, we strove for a relatively fast and reliable system so that a user's experience is not negatively impacted by the diversity of logic that gets executed behind the hood. We cared for the continuity of a system even in case of non-availability of one of the sensors.


