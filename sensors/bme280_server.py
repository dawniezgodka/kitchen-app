import socketserver
import json

from bme280 import readBME280ID, readBME280All

class BME280ServerHandler(socketserver.BaseRequestHandler):
	
	def handle(self):
	
		chip_id, chip_version = readBME280ID()
		temperature, pressure, humidity = readBME280All()
		
		data = json.dumps({
			'id': str(chip_id),
			'temperature': str(temperature),
			'pressure': str(pressure)
		})
		
		self.request.sendall(bytes(data + '\n', 'utf-8'))

def main():
	
	host = '127.0.0.1'
	port = 1024
	
	server = socketserver.TCPServer((host, port), BME280ServerHandler)
	
	server.serve_forever()

if __name__ == "__main__":
	
    main()