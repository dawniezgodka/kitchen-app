import socketserver
import json

def read_onewire_devices():
	file = open('/sys/devices/w1_bus_master1/w1_master_slaves')
	w1_slaves = file.readlines()
	file.close()
	return [w1_slave.strip() for w1_slave in w1_slaves]
	
def read_onewire_device(device_id):
	file = open('/sys/bus/w1/devices/{}/w1_slave'.format(device_id))
	filecontent = file.read().strip()
	file.close()
	temperature = float(filecontent[filecontent.index('t=')+2:]) / 1000
	return temperature
	

class OneWireServerHandler(socketserver.BaseRequestHandler):
	
	def handle(self):
		
		# we are not using read_onewire_devices because there is only one sensor with a constant id
		device_id = '28-04163410e2ff'
		temperature = read_onewire_device(device_id)
		
		data = json.dumps({
			'id': device_id,
			'temperature': str(temperature)
		})
		
		self.request.sendall(bytes(data + '\n', 'utf-8'))

def main():
	
	host = '127.0.0.1'
	port = 1025
	
	server = socketserver.TCPServer((host, port), OneWireServerHandler)
	
	server.serve_forever()

if __name__ == "__main__":
	
    main()