from WaterBoilerTemperatureSensor import WaterBoilerTemperatureSensor
from const import BASIS_PORT

def main():
	
	water_boiler_1 = WaterBoilerTemperatureSensor(1, 'first sensor.', 2, BASIS_PORT)
	water_boiler_2 = WaterBoilerTemperatureSensor(2, 'second one.', 3, BASIS_PORT+1)

if __name__ == "__main__":
	
    main()